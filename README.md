# Volksentscheide Berlin

## Name
Volksentscheide Berlin

## Description
some statistics and visualizations on plebiscites in Berlin

## Installation
`git clone git@gitlab.com:Therac-25/volksentscheide-berlin.git`

## Usage
Navigate to the project folder: `cd volksentscheide-berlin`

To run the project: `. start_project`

In the browser, navigate to [http://localhost:8890/tree](http://localhost:8890/tree) and select:

* `all_plebiscites.ipynb` for a general overview of the different plebiscites
* `energietisch.ipynb` for correlations between Energietisch (2013) and party results, including graphics
* `dwe.ipynb` for correlations between DWE (2021) and party results, including graphics

Alternatively, run it on mybinder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Therac-25%2Fvolksentscheide-berlin/main)
